<?php

namespace Drupal\trustpilot_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\trustpilot_api\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trustpilot_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'trustpilot_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('trustpilot_api.settings');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Client Settings'),
      'api_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('Trustpilot API Key'),
        '#description' => $this->t('Recommended this value be kept in a settings*.php file and not stored in config. <pre>@example</pre>', [
          '@example' => '$config["trustpilot_api.settings"]["api_key"] = "";',
        ]),
        '#default_value' => $config->get('api_key'),
      ],
      'api_secret' => [
        '#type' => 'textfield',
        '#title' => $this->t('Trustpilot API Secret'),
        '#description' => $this->t('Required to use private endpoints. Recommended this value be kept in a settings*.php file and not stored in config. <pre>@example</pre>', [
          '@example' => '$config["trustpilot_api.settings"]["api_secret"] = "";',
        ]),
        '#default_value' => $config->get('api_secret'),
      ],
      'oauth_email' => [
        '#type' => 'textfield',
        '#title' => $this->t('OAuth Email'),
        '#description' => $this->t('Your Trustpilot b2b login email. Required to use private endpoints. <a href="https://developers.trustpilot.com/authentication#password">Documentation</a>'),
        '#default_value' => $config->get('oauth_email'),
      ],
      'oauth_password' => [
        '#type' => 'textfield',
        '#title' => $this->t('OAuth Password'),
        '#description' => $this->t('Your Trustpilot b2b login password. Required to use private endpoints. Recommended this value be kept in a settings*.php file and not stored in config. <pre>@example</pre>', [
          '@example' => '$config["trustpilot_api.settings"]["oauth_password"] = "";',
        ]),
        '#default_value' => $config->get('oauth_password'),
      ],
      'business_unit_id' => [
        '#type' => 'textfield',
        '#title' => $this->t('Default Business Unit Id'),
        '#description' => $this->t('This can be overridden per request.'),
        '#default_value' => $config->get('business_unit_id'),
      ],
      'logging_enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable logging of API calls.'),
        '#description' => $this->t('Record the effective URL and other details about performed API calls to Drupal logs.'),
        '#default_value' => $config->get('logging_enabled'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trustpilot_api.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_secret', $form_state->getValue('api_secret'))
      ->set('oauth_email', $form_state->getValue('oauth_email'))
      ->set('oauth_password', $form_state->getValue('oauth_password'))
      ->set('business_unit_id', $form_state->getValue('business_unit_id'))
      ->set('logging_enabled', $form_state->getValue('logging_enabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
