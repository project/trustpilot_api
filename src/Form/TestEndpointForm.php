<?php

namespace Drupal\trustpilot_api\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\trustpilot_api\TrustpilotApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestForm.
 *
 * @package Drupal\trustpilot_api\Form
 */
class TestEndpointForm extends FormBase {

  /**
   * Trustpilot Api client.
   *
   * @var \Drupal\trustpilot_api\TrustpilotApiClientInterface
   */
  protected $client;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\trustpilot_api\TrustpilotApiClientInterface $trustpilot_api_client
   *   Trustpilot Api client.
   */
  public function __construct(TrustpilotApiClientInterface $trustpilot_api_client) {
    $this->client = $trustpilot_api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('trustpilot_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trustpilot_api_test_endpoint';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $endpoint_id = $this->getRequest()->get('endpoint_id');
    try {
      $endpoint = $this->client->getEndpointPluginManager()->createInstance($endpoint_id);
    }
    catch (PluginNotFoundException $exception) {
      $this->messenger()->addError($this->t('Endpoint ID Not Found - :endpoint_id', [':endpoint_id' => $endpoint_id]));
      return [];
    }

    $documentation_url = $endpoint->getPluginDefinition()['documentationUrl'];
    $form['endpoint_id'] = [
      '#type' => 'value',
      '#value' => $endpoint_id,
    ];
    $form['endpoint_params'] = [
      '#type' => 'fieldset',
      '#title' => $endpoint->getPluginName(),
      '#description' => Markup::create("<a href='{$documentation_url}'>{$documentation_url}</a>"),
      '#tree' => TRUE,
    ];
    $endpoint->setRequestParams();
    foreach ($endpoint->getRequestParams() as $param => $default_value) {
      $form['endpoint_params'][$param] = [
        '#type' => 'textfield',
        '#title' => $param,
        '#default_value' => $default_value,
        '#required' => TRUE,
      ];
    }
    foreach ($endpoint->getDefaultRequestParams() as $param => $default_value) {
      $form['endpoint_params'][$param] = [
        '#type' => 'textfield',
        '#title' => $param,
        '#default_value' => is_array($default_value) ? implode(',', $default_value) : $default_value,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['test_endpoints'] = [
      '#type' => 'submit',
      '#value' => $this->t('Perform Test'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $endpoint_id = $form_state->getValue('endpoint_id');
    $endpoint_params = $form_state->getValue('endpoint_params');
    try {
      $endpoint = $this->client->getEndpointPluginManager()->createInstance($endpoint_id);
    }
    catch (PluginNotFoundException $exception) {
      $messenger->addError($this->t('Endpoint ID Not Found - :endpoint_id', [':endpoint_id' => $endpoint_id]));
      return;
    }

    $result = $this->client->request($endpoint, $endpoint_params);
    $messenger->addStatus($this->t(':endpoint_id - :result', [
      ':endpoint_id' => $endpoint_id,
      ':result' => empty($result) ? $this->t('Failed') : $this->t('Success'),
    ]));
    $messenger->addStatus($this->arrayToSafeMarkup($result));
  }

  /**
   * Convert array to safe markup for formatted messages.
   *
   * @param array $array
   *   Array to convert to markup.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   Markup string.
   */
  protected function arrayToSafeMarkup(array $array) {
    return Markup::create('<pre>' . print_r($array, 1) . '</pre>');
  }

}
