<?php

namespace Drupal\trustpilot_api;

/**
 * Interface TrustpilotClientInterface.
 *
 * @package Drupal\trustpilot_api\Service\Trustpilot
 */
interface TrustpilotApiClientInterface {

  /**
   * Get the http client.
   *
   * @return \GuzzleHttp\Client
   *   Http client.
   */
  public function getHttpClient();

  /**
   * Get the plugin manager for endpoints.
   *
   * @return \Drupal\trustpilot_api\EndpointPluginManager
   *   Endpoint plugin manager.
   */
  public function getEndpointPluginManager();

  /**
   * Perform a request to trustpilot API.
   *
   * @param \Drupal\trustpilot_api\EndpointPluginInterface $endpoint
   *   Endpoint plugin instance.
   * @param array $params
   *   Array of endpoint parameters for the request.
   *
   * @return array
   *   Json response decoded as array.
   */
  public function request(EndpointPluginInterface $endpoint, array $params = []);

  /**
   * Whether or not the client can authorize for private requests.
   *
   * @return bool
   *   True if client has required secrets, false otherwise.
   */
  public function canAuthorizePrivate(): bool;

}
