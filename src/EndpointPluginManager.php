<?php

namespace Drupal\trustpilot_api;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class EndpointPluginManager.
 *
 * @package Drupal\trustpilot_api
 */
class EndpointPluginManager extends DefaultPluginManager {

  /**
   * DataServicePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/TrustpilotApi/Endpoint', $namespaces, $module_handler, 'Drupal\trustpilot_api\EndpointPluginInterface', 'Drupal\trustpilot_api\Annotation\Endpoint');
    $this->alterInfo('trustpilot_api_endpoint_info');
    $this->setCacheBackend($cache, 'trustpilot_api_endpoint');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    ksort($definitions);
    return $definitions;
  }

  /**
   * Create an instance of a plugin.
   *
   * @param string $plugin_id
   *   The id of the setup plugin.
   * @param array $configuration
   *   Configuration data for the setup plugin.
   *
   * @return \Drupal\trustpilot_api\EndpointPluginInterface
   *   Instance of the endpoint plugin.
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return $this->getFactory()->createInstance($plugin_id, $configuration);
  }

}
