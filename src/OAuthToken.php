<?php

namespace Drupal\trustpilot_api;

/**
 * Class OAuthToken.
 *
 * @package Drupal\trustpilot_api
 */
class OAuthToken implements OAuthTokenInterface {

  /**
   * Access token.
   *
   * @var string
   */
  protected $accessToken = '';

  /**
   * Refresh token.
   *
   * @var string
   */
  protected $refreshToken = '';

  /**
   * Lifespan of the token in seconds.
   *
   * @var int
   */
  protected $lifespan = 0;

  /**
   * Timestamp for when the token will be expired.
   *
   * @var int
   */
  protected $expiresAt = 0;

  /**
   * OAuthToken constructor.
   *
   * @param string $access_token
   *   Access token.
   * @param string $refresh_token
   *   Refresh token.
   * @param int $token_lifespan
   *   Lifespan of the token in seconds.
   */
  public function __construct(string $access_token, string $refresh_token, int $token_lifespan) {
    $this->accessToken = $access_token;
    $this->refreshToken = $refresh_token;
    $this->lifespan = $token_lifespan;
    $this->expiresAt = time() + $token_lifespan;
  }

  /**
   * {@inheritdoc}
   */
  public function serialize(): string {
    return \serialize(\get_object_vars($this));
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize(string $data): self {
    $array = \unserialize($data, ['allowed_classes' => FALSE]);
    foreach ($array as $key => $value) {
      $this->{$key} = $value;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function accessToken(): string {
    return $this->accessToken;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(): string {
    return $this->refreshToken;
  }

  /**
   * {@inheritdoc}
   */
  public function lifespan(): int {
    return $this->lifespan;
  }

  /**
   * {@inheritdoc}
   */
  public function expiresAt(): int {
    return $this->expiresAt;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpired(): bool {
    // Consider the token expired 10 minutes early.
    return (time() + 600) >= $this->expiresAt();
  }

}
