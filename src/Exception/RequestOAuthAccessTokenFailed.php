<?php

namespace Drupal\trustpilot_api\Exception;

/**
 * Class RequestOAuthAccessTokenFailed.
 *
 * @package Drupal\trustpilot_api\Exception
 */
class RequestOAuthAccessTokenFailed extends \RuntimeException {}
