<?php

namespace Drupal\trustpilot_api\Exception;

/**
 * Class EndpointRequiredOptionsMissing.
 *
 * @package Drupal\trustpilot_api\Exception
 */
class EndpointRequiredOptionsMissing extends \RuntimeException {}
