<?php

namespace Drupal\trustpilot_api;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface EndpointPluginInterface.
 *
 * @package Drupal\trustpilot_api
 */
interface EndpointPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Endpoint authorization type indicating api_key required for requests.
   */
  const ENDPOINT_AUTH_TYPE_KEY = 'key';

  /**
   * Endpoint authorization type indicating oauth tokens required for requests.
   */
  const ENDPOINT_AUTH_TYPE_OAUTH = 'oauth';

  /**
   * Human readable plugin name.
   *
   * @return string
   *   Human readable plugin name.
   */
  public function getPluginName();

  /**
   * Get the prepared request path.
   *
   * @return string
   *   Endpoint request path.
   */
  public function getRequestPath(): string;

  /**
   * Get the method for the endpoint request.
   *
   * @return string
   *   HTTP request method like: GET, POST, etc.
   */
  public function getRequestMethod(): string;

  /**
   * Get the endpoint's authorization type.
   *
   * @return string
   *   Endpoint authorization type.
   */
  public function getRequestAuthType(): string;

  /**
   * Get array of required request parameter keys.
   *
   * @return array
   *   Array of required request parameter keys.
   */
  public function getRequiredParams(): array;

  /**
   * Get array of default request parameters.
   *
   * @return array
   *   Default endpoint request parameters.
   */
  public function getDefaultRequestParams(): array;

  /**
   * Get array of optional request headers.
   *
   * @return array
   *  Array of headers.
   */
  public function getHeaders(): array;

  /**
   * Get populated request parameters.
   *
   * @return array
   *   Request specific parameters.
   */
  public function getRequestParams(): array;

  /**
   * Populate the request parameters.
   *
   * @param array $params
   *   Request specific parameters.
   */
  public function setRequestParams(array $params = []);

  /**
   * Whether or not the endpoint can perform the request in its current state.
   *
   * @return bool
   *   True if the request is ready to be performed.
   */
  public function canPerformRequest(): bool;

}
