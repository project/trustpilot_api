<?php

namespace Drupal\trustpilot_api\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\trustpilot_api\EndpointPluginInterface;

/**
 * Class Endpoint.
 *
 * @Annotation
 */
class Endpoint extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin name.
   *
   * @var string
   */
  public $name;

  /**
   * Endpoint path as a pattern w/ replaceable values.
   *
   * @var string
   */
  public $path;

  /**
   * Endpoint request method.
   *
   * @var string
   */
  public $method = 'GET';

  /**
   * Endpoint's authorization type: key | oauth.
   *
   * @var string
   */
  public $authType = EndpointPluginInterface::ENDPOINT_AUTH_TYPE_KEY;

  /**
   * Endpoint documentation URL.
   *
   * @var string
   */
  public $documentationUrl = '';

  /**
   * Array of parameter keys that are required for the endpoint.
   *
   * @var array
   */
  public $requiredParams = [];

  /**
   * Array of endpoint default request parameters.
   *
   * @var array
   */
  public $defaultRequestParams = [];

  /**
   * Array of optional headers to use for requests made for this endpoint.
   *
   * @var array
   */
  public $headers = [];

}
