<?php

namespace Drupal\trustpilot_api;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\trustpilot_api\Exception\EndpointRequiredOptionsMissing;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EndpointPluginBase.
 *
 * @package Drupal\trustpilot_api
 */
abstract class EndpointPluginBase extends PluginBase implements EndpointPluginInterface {

  /**
   * Module config values.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $trustpilotApiConfig;

  /**
   * Endpoint request options.
   *
   * @var array
   */
  protected $requestParams = [];

  /**
   * EndpointPluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->trustpilotApiConfig = $config_factory->get('trustpilot_api.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredParams(): array {
    return $this->pluginDefinition['requiredParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRequestParams(): array {
    return $this->pluginDefinition['defaultRequestParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaders(): array {
    return $this->pluginDefinition['headers'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestPath(): string {
    $replacements = [];
    $options = $this->getRequestParams();
    foreach ($this->getRequiredParams() as $required_key) {
      if (!isset($options[$required_key])) {
        throw new EndpointRequiredOptionsMissing($required_key);
      }
      $replacements["[{$required_key}]"] = $options[$required_key];
    }
    return strtr($this->pluginDefinition['path'], $replacements);
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestMethod(): string {
    return \strtoupper($this->pluginDefinition['method']);
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestAuthType(): string {
    return $this->pluginDefinition['authType'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestParams(): array {
    return $this->requestParams;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestParams(array $params = []) {
    $params = array_replace($this->getDefaultRequestParams(), $params);

    // If the endpoint requires the business unit id, and we don't have one in
    // the $options array, use the site wide default value.
    if (in_array('businessUnitId', $this->getRequiredParams()) && empty($params['businessUnitId'])) {
      $params['businessUnitId'] = $this->trustpilotApiConfig->get('business_unit_id');
    }

    $this->requestParams = array_filter($params, function ($value) {
      return $value !== '' && $value !== [];
    });
  }

  /**
   * {@inheritdoc}
   */
  public function canPerformRequest(): bool {
    $options = $this->getRequestParams();
    foreach ($this->getRequiredParams() as $required_key) {
      if (!isset($options[$required_key])) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
