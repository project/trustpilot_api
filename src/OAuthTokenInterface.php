<?php

namespace Drupal\trustpilot_api;

/**
 * Interface OAuthTokenInterface.
 *
 * @package Drupal\trustpilot_api
 */
interface OAuthTokenInterface {

  /**
   * Access token for the OAuth2 Authorization.
   *
   * @return string
   *   Access token.
   */
  public function accessToken(): string;

  /**
   * Refresh token for OAuth2 Authorization.
   *
   * @return string
   *   Refresh token.
   */
  public function refreshToken(): string;

  /**
   * Lifespan of the token in seconds.
   *
   * @return int
   *   Lifespan of the token in seconds.
   */
  public function lifespan(): int;

  /**
   * Timestamp of expiration.
   *
   * @return int
   *   Expiration unix timestamp.
   */
  public function expiresAt(): int;

  /**
   * Whether or not the token is expired.
   *
   * @return bool
   *   True if expiresAt() is greater than now, otherwise false.
   */
  public function isExpired(): bool;

  /**
   * Serialize the important object properties into an array.
   *
   * @return string
   *   Serialized string.
   */
  public function serialize(): string;

  /**
   * Unserialize given values into object property values.
   *
   * @param string $data
   *   Serialized string.
   *
   * @return $this
   *   Object instance.
   */
  public function unserialize(string $data): self;

}
