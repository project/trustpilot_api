<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Insert or update (if they already exist) a batch of products.
 *
 * @Endpoint(
 *   id = "private_products_upsert",
 *   name = @Translation("Private Products Upsert"),
 *   path = "private/business-units/[businessUnitId]/products",
 *   method = "POST",
 *   authType = "oauth",
 *   documentationUrl= "https://developers.trustpilot.com/private-products-api#batch-upsert-products",
 *   requiredParams = {
 *     "businessUnitId",
 *     "products",
 *   },
 * )
 */
class PrivateProductsUpsert extends EndpointPluginBase {}
