<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get imported product reviews by business unit id, SKUs and language.
 *
 * @Endpoint(
 *   id = "product_reviews_get_imported",
 *   name = @Translation("Product Reviews Get Imported"),
 *   path = "product-reviews/business-units/[businessUnitId]/imported-reviews",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/product-reviews-api#get-imported-product-reviews",
 *   requiredParams = {
 *     "businessUnitId",
 *     "skus",
 *   },
 *   defaultRequestParams = {
 *     "perPage" = "",
 *     "language" = "",
 *     "page" = "",
 *   },
 * )
 */
class ProductReviewsGetImported extends EndpointPluginBase {}
