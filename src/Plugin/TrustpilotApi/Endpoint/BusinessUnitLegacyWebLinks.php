<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's legacy web links.
 *
 * @Endpoint(
 *   id = "business_unit_legacy_web_links",
 *   name = @Translation("Business Unit Legacy Web Links"),
 *   path = "business-units/[businessUnitId]/web-links",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-legacy-api#get-a-business-unit's-web-links",
 *   requiredParams = {
 *     "businessUnitId",
 *     "locale",
 *   },
 * )
 */
class BusinessUnitLegacyWebLinks extends EndpointPluginBase {}
