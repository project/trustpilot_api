<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a single category.
 *
 * @Endpoint(
 *   id = "categories_get",
 *   name = @Translation("Categories Get One"),
 *   path = "categories/[categoryId]",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/categories-api#get-category",
 *   requiredParams = {
 *     "country",
 *     "categoryId",
 *   },
 *   defaultRequestParams = {
 *     "locale" = "",
 *   }
 * )
 */
class CategoriesGet extends EndpointPluginBase {}
