<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's profile promotion.
 *
 * @Endpoint(
 *   id = "business_unit_profile_promotion",
 *   name = @Translation("Business Unit Profile Promotion"),
 *   path = "business-units/[businessUnitId]/profilepromotion",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-profile-promotion-of-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   }
 * )
 */
class BusinessUnitProfilePromotion extends EndpointPluginBase {}
