<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get products.
 *
 * @Endpoint(
 *   id = "private_products_get",
 *   name = @Translation("Private Products Get"),
 *   path = "private/business-units/[businessUnitId]/products",
 *   authType = "oauth",
 *   documentationUrl= "https://developers.trustpilot.com/private-products-api#get-products",
 *   requiredParams = {
 *     "businessUnitId",
 *   },
 *   defaultRequestParams = {
 *     "locale" = "",
 *     "perPage" = "",
 *     "skus" = {},
 *     "page" = "",
 *   },
 * )
 */
class PrivateProductsGet extends EndpointPluginBase {}
