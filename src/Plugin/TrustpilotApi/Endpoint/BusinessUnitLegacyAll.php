<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Business units legacy list of all business units, including without reviews.
 *
 * @Endpoint(
 *   id = "business_unit_legacy_all",
 *   name = @Translation("Business Unit Legacy All"),
 *   path = "business-units/all",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-legacy-api#get-a-list-of-all-business-units",
 *   defaultRequestParams = {
 *     "country" = "",
 *     "perPage" = "",
 *     "page" = "",
 *   },
 * )
 */
class BusinessUnitLegacyAll extends EndpointPluginBase {}
