<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Gets a list of consumer profiles.
 *
 * @Endpoint(
 *   id = "consumer_profile_list",
 *   name = @Translation("Consumer Profiles List"),
 *   path = "consumers/profile/bulk",
 *   method = "POST",
 *   documentationUrl = "https://documentation-apidocumentation.trustpilot.com/consumer-profile-api#get-a-list-of-consumer-profiles",
 *   requiredParams = {
 *     "consumerIds",
 *   },
 * )
 */
class ConsumerProfileList extends EndpointPluginBase {}
