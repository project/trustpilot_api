<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's categories.
 *
 * @Endpoint(
 *   id = "business_unit_categories",
 *   name = @Translation("Business Unit Categories"),
 *   path = "business-units/[businessUnitId]/categories",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#list-categories-for-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   },
 *   defaultRequestParams = {
 *     "local" = "",
 *     "country" = "",
 *   }
 * )
 */
class BusinessUnitCategories extends EndpointPluginBase {}
