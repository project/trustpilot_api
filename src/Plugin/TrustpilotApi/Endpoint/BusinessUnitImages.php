<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's images.
 *
 * @Endpoint(
 *   id = "business_unit_images",
 *   name = @Translation("Business Unit Images"),
 *   path = "business-units/[businessUnitId]/images",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-images-of-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   }
 * )
 */
class BusinessUnitImages extends EndpointPluginBase {}
