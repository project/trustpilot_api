<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a list of all categories.
 *
 * @Endpoint(
 *   id = "categories_list",
 *   name = @Translation("Categories List"),
 *   path = "categories",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/categories-api#list-categories",
 *   defaultRequestParams = {
 *     "locale" = "",
 *     "country" = "",
 *     "parentId" = "",
 *   }
 * )
 */
class CategoriesList extends EndpointPluginBase {}
