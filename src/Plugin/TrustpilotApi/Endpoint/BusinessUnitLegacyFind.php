<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Business units legacy search.
 *
 * @Endpoint(
 *   id = "business_unit_legacy_find",
 *   name = @Translation("Business Unit Legacy Find"),
 *   path = "business-units/find",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-legacy-api#find-a-business-unit",
 *   requiredParams = {
 *     "name",
 *   },
 * )
 */
class BusinessUnitLegacyFind extends EndpointPluginBase {}
