<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a list of business units in a specific category.
 *
 * @Endpoint(
 *   id = "categories_business_units",
 *   name = @Translation("Categories Business Units"),
 *   path = "categories/[categoryId]/business-units",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/categories-api#list-business-units-in-category",
 *   requiredParams = {
 *     "categoryId"
 *   },
 *   defaultRequestParams = {
 *     "perPage" = "",
 *     "locale" = "",
 *     "country" = "",
 *     "page" = "",
 *   }
 * )
 */
class CategoriesBusinessUnits extends EndpointPluginBase {}
