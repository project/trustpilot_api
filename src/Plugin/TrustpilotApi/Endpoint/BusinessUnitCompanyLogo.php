<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's company logo.
 *
 * @Endpoint(
 *   id = "business_unit_company_logo",
 *   name = @Translation("Business Unit Company Logo"),
 *   path = "business-units/[businessUnitId]/images/logo",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-business-unit-company-logo",
 *   requiredParams = {
 *     "businessUnitId",
 *   }
 * )
 */
class BusinessUnitCompanyLogo extends EndpointPluginBase {}
