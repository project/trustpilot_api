<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get summaries of product reviews for multiple SKUs.
 *
 * @Endpoint(
 *   id = "product_reviews_summaries",
 *   name = @Translation("Product Reviews Summaries"),
 *   path = "product-reviews/business-units/[businessUnitId]/batch-summaries",
 *   method = "POST",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/product-reviews-api#get-batch-product-reviews-summaries",
 *   requiredParams = {
 *     "businessUnitId",
 *     "skus",
 *   },
 * )
 */
class ProductReviewsSummaries extends EndpointPluginBase {}
