<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Business units legacy list of all business units, with reviews.
 *
 * @Endpoint(
 *   id = "business_unit_legacy_list",
 *   name = @Translation("Business Unit Legacy List"),
 *   path = "business-units",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-legacy-api#get-a-list-of-business-units",
 *   defaultRequestParams = {
 *     "country" = "",
 *     "perPage" = "",
 *     "page" = "",
 *   },
 * )
 */
class BusinessUnitLegacyList extends EndpointPluginBase {}
