<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's customer guarantee.
 *
 * @Endpoint(
 *   id = "business_unit_customer_guarantee",
 *   name = @Translation("Business Unit Customer Guarantee"),
 *   path = "business-units/[businessUnitId]/customerguarantee",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-customer-guarantee-of-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   }
 * )
 */
class BusinessUnitCustomerGuarantee extends EndpointPluginBase {}
