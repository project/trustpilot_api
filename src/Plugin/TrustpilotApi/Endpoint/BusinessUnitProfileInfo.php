<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's profile info.
 *
 * @Endpoint(
 *   id = "business_unit_profile_info",
 *   name = @Translation("Business Unit Profile Info"),
 *   path = "business-units/[businessUnitId]/profileinfo",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-profile-info-of-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   }
 * )
 */
class BusinessUnitProfileInfo extends EndpointPluginBase {}
