<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's legacy public info.
 *
 * @Endpoint(
 *   id = "business_unit_legacy_public_info",
 *   name = @Translation("Business Unit Legacy Public Info"),
 *   path = "business-units/[businessUnitId]",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-legacy-api#get-public-business-unit",
 *   requiredParams = {
 *     "businessUnitId",
 *   },
 * )
 */
class BusinessUnitLegacyPublicInfo extends EndpointPluginBase {}
