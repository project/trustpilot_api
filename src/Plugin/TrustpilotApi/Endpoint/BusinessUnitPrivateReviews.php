<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's private reviews.
 *
 * @Endpoint(
 *   id = "business_unit_private_reviews",
 *   name = @Translation("Business Unit Private Reviews"),
 *   path = "private/business-units/[businessUnitId]/reviews",
 *   authType = "oauth",
 *   documentationUrl= "https://developers.trustpilot.com/business-units-api#business-unit-private-reviews",
 *   requiredParams = {
 *     "businessUnitId",
 *   },
 *   defaultRequestParams = {
 *     "orderBy" = "",
 *     "username" = "",
 *     "perPage" = "",
 *     "referralEmail" = "",
 *     "language" = "",
 *     "ignoreTagValueCase" = "",
 *     "responded" = "",
 *     "findReviewer" = "",
 *     "startDateTime" = "",
 *     "endDateTime" = "",
 *     "internalLocationId" = "",
 *     "referenceId" = "",
 *     "reported" = "",
 *     "tagValue" = "",
 *     "source" = "",
 *     "stars" = {},
 *     "tagGroup" = "",
 *     "page" = "",
 *   },
 * )
 */
class BusinessUnitPrivateReviews extends EndpointPluginBase {}
