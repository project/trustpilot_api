<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Search for business units.
 *
 * @Endpoint(
 *   id = "business_unit_search",
 *   name = @Translation("Business Unit Search"),
 *   path = "business-units/search",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#search-for-business-units",
 *   requiredParams = {
 *     "query",
 *   },
 *   defaultRequestParams = {
 *     "perpage" = "",
 *     "page" = "",
 *     "country" = "",
 *   }
 * )
 */
class BusinessUnitSearch extends EndpointPluginBase {}
