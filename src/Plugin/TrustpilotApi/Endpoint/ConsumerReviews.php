<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Gets the reviews written by the individual consumer.
 *
 * @Endpoint(
 *   id = "consumer_reviews",
 *   name = @Translation("Consumer Reviews"),
 *   path = "consumers/[consumerId]/reviews",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/consumer-api",
 *   requiredParams = {
 *     "consumerId",
 *   },
 *   defaultRequestParams = {
 *     "orderBy" = "",
 *     "perPage" = "",
 *     "businessUnitId" = "",
 *     "language" = "",
 *     "internalLocationId" = "",
 *     "stars" = "",
 *     "includeReportedReviews" = "",
 *     "page" = "",
 *   }
 * )
 */
class ConsumerReviews extends EndpointPluginBase {}
