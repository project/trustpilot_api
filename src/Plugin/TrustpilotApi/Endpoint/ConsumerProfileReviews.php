<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Gets the profile information of the consumer with the number of reviews.
 *
 * @Endpoint(
 *   id = "consumer_profile_reviews",
 *   name = @Translation("Consumer Profile Reviews"),
 *   path = "consumers/[consumerId]",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/consumer-profile-api#get-the-profile-of-the-consumer(with-#reviews-and-weblinks)",
 *   requiredParams = {
 *     "consumerId",
 *   },
 * )
 */
class ConsumerProfileReviews extends EndpointPluginBase {}
