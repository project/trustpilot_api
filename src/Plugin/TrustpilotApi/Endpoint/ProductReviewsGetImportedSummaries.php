<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a summary of imported product reviews for a single or multiple SKUs.
 *
 * @Endpoint(
 *   id = "product_reviews_get_imported_summaries",
 *   name = @Translation("Product Reviews Get Imported Summaries"),
 *   path = "product-reviews/business-units/[businessUnitId]/imported-reviews-summaries",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/product-reviews-api#get-imported-product-reviews-summary",
 *   requiredParams = {
 *     "businessUnitId",
 *     "sku",
 *   },
 * )
 */
class ProductReviewsGetImportedSummaries extends EndpointPluginBase {}
