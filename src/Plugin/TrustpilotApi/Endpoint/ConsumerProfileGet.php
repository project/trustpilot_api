<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Gets the profile information of the consumer.
 *
 * @Endpoint(
 *   id = "consumer_profile_get",
 *   name = @Translation("Consumer Profile Get"),
 *   path = "consumers/[consumerId]/profile",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/consumer-profile-api#get-the-profile-of-the-consumer",
 *   requiredParams = {
 *     "consumerId",
 *   },
 * )
 */
class ConsumerProfileGet extends EndpointPluginBase {}
