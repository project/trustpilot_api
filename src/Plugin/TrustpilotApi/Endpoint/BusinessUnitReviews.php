<?php

namespace Drupal\trustpilot_api\Plugin\TrustpilotApi\Endpoint;

use Drupal\trustpilot_api\EndpointPluginBase;

/**
 * Get a business unit's reviews.
 *
 * @Endpoint(
 *   id = "business_unit_reviews",
 *   name = @Translation("Business Unit Reviews"),
 *   path = "business-units/[businessUnitId]/reviews",
 *   documentationUrl= "https://documentation-apidocumentation.trustpilot.com/business-units-api#get-a-business-unit's-reviews",
 *   requiredParams = {
 *     "businessUnitId",
 *   },
 *   defaultRequestParams = {
 *     "orderBy" = "",
 *     "perPage" = "",
 *     "language" = "",
 *     "internalLocationId" = "",
 *     "tagValue" = "",
 *     "stars" = {},
 *     "tagGroup" = "",
 *     "includeReportedReviews" = "",
 *     "page" = "",
 *   },
 * )
 */
class BusinessUnitReviews extends EndpointPluginBase {}
