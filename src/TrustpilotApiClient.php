<?php

namespace Drupal\trustpilot_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\trustpilot_api\Exception\RequestOAuthAccessTokenFailed;
use GuzzleHttp\TransferStats;
use GuzzleHttp\RequestOptions;

/**
 * Class TrustpilotClient.
 *
 * @package Drupal\trustpilot_api\Service
 */
class TrustpilotApiClient implements TrustpilotApiClientInterface {

  /**
   * Module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * API client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Endpoint plugin manager.
   *
   * @var \Drupal\trustpilot_api\EndpointPluginManager
   */
  protected $endpointPluginManager;

  /**
   * Json service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $json;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Http client options.
   *
   * @var array
   */
  protected $clientOptions = [];

  /**
   * API version number.
   *
   * @var string
   */
  protected $version = 'v1';

  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Drupal core config factory.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   Drupal Guzzle HTTP Client factory.
   * @param \Drupal\trustpilot_api\EndpointPluginManager $endpoint_plugin_manager
   *   Endpoint plugin manager.
   * @param \Drupal\Component\Serialization\Json $json
   *   Json service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Logger channel factory service.
   */
  public function __construct(ConfigFactory $config_factory, ClientFactory $http_client_factory, EndpointPluginManager $endpoint_plugin_manager, Json $json, LoggerChannelFactoryInterface $logger_channel_factory, CacheBackendInterface $cache_backend) {
    $this->config = $config_factory->get('trustpilot_api.settings');
    $this->clientOptions = [
      'base_uri' => "https://api.trustpilot.com/{$this->version}/",
      'headers' => [
        'apiKey' => $this->config->get('api_key'),
      ],
    ];
    $this->httpClient = $http_client_factory->fromOptions($this->clientOptions);
    $this->endpointPluginManager = $endpoint_plugin_manager;
    $this->json = $json;
    $this->logger = $logger_channel_factory->get('trustpilot_api');
    $this->cache = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public function getHttpClient() {
    return $this->httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointPluginManager() {
    return $this->endpointPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function request(EndpointPluginInterface $endpoint, array $params = []) {
    $endpoint->setRequestParams($params);
    $query = $endpoint->getRequestParams();
    $headers = $endpoint->getHeaders();

    if ($endpoint->getRequestAuthType() == EndpointPluginInterface::ENDPOINT_AUTH_TYPE_OAUTH) {
      $token = $this->getAccessToken();
      $query['token'] = $token->accessToken();
    }

    $effective_uri = '';
    $options = [
      'http_errors' => FALSE,
      'query' => $query,
      'headers' => $headers,
      'on_stats' => function (TransferStats $stats) use (&$effective_uri) {
        $effective_uri = $stats->getEffectiveUri();
      },
    ];

    if ($endpoint->getRequestMethod() === 'POST') {
      $options[RequestOptions::JSON] = $params;
    }

    $response = $this->getHttpClient()->request($endpoint->getRequestMethod(), $endpoint->getRequestPath(), $options);

    if ($this->config->get('logging_enabled')) {
      $this->logger->info('Request to %endpoint_id, Status %status - %effective_uri', [
        '%endpoint_id' => $endpoint->getPluginId(),
        '%status' => $response->getStatusCode(),
        '%effective_uri' => $effective_uri,
      ]);
    }

    if ($response->getStatusCode() === 200) {
      return $this->json::decode($response->getBody());
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function canAuthorizePrivate(): bool {
    return (
      $this->config->get('api_key') &&
      $this->config->get('api_secret') &&
      $this->config->get('oauth_email') &&
      $this->config->get('oauth_password')
    );
  }

  /**
   * Save an access token to the cache backend.
   *
   * @param \Drupal\trustpilot_api\OAuthTokenInterface $token
   *   Cached access token instance.
   */
  protected function saveAccessToken(OAuthTokenInterface $token) {
    $this->cache->set('trustpilot_api.oauth_access_token', $token, $token->expiresAt());
  }

  /**
   * Retrieve a valid access token.
   *
   * @return \Drupal\trustpilot_api\OAuthTokenInterface
   *   Valid access token instance.
   */
  protected function getAccessToken() {
    $cache = $this->cache->get('trustpilot_api.oauth_access_token', TRUE);
    /** @var \Drupal\trustpilot_api\OAuthTokenInterface $token */
    if ($cache) {
      $token = $cache->data;
    }

    // If no existing previous token, request a new one.
    if (!isset($token)) {
      $token = $this->requestNewAccessToken();
      $this->saveAccessToken($token);
    }

    // Token is expired, refresh it.
    if ($token->isExpired()) {
      $token = $this->refreshAccessToken($token);
      $this->saveAccessToken($token);
    }

    return $token;
  }

  /**
   * Request a new OAuth access token from Trustpilot.
   *
   * @return \Drupal\trustpilot_api\OAuthToken
   *   Newly instantiated access token.
   */
  protected function requestNewAccessToken() {
    $response = $this->requestAccessToken([
      'grant_type' => 'password',
      'username' => $this->config->get('oauth_email'),
      'password' => $this->config->get('oauth_password'),
    ]);

    if ($response->getStatusCode() === 200) {
      $values = $this->json::decode($response->getBody());
      return new OAuthToken($values['access_token'], $values['refresh_token'], $values['expires_in']);
    }

    throw new RequestOAuthAccessTokenFailed();
  }

  /**
   * Refresh an existing access token.
   *
   * @param \Drupal\trustpilot_api\OAuthTokenInterface $token
   *   Existing access token with a refreshToken value.
   *
   * @return \Drupal\trustpilot_api\OAuthToken
   *   Newly instantiated access token.
   */
  protected function refreshAccessToken(OAuthTokenInterface $token) {
    $response = $this->requestAccessToken([
      'grant_type' => 'refresh_token',
      'refresh_token' => $token->refreshToken(),
    ]);

    if ($response->getStatusCode() === 200) {
      $values = $this->json::decode($response->getBody());
      return new OAuthToken($values['access_token'], $values['refresh_token'], $values['expires_in']);
    }

    throw new RequestOAuthAccessTokenFailed();
  }

  /**
   * Perform a request to the Trustpilot OAuth token exchange.
   *
   * @param array $params
   *   Appropriate values for the grant_type, including the grant_type itself.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Full response from the request for an access token.
   */
  protected function requestAccessToken(array $params = []) {
    $params = array_replace(['grant_type' => 'password'], $params);
    $response = $this->getHttpClient()->post('https://api.trustpilot.com/v1/oauth/oauth-business-users-for-applications/accesstoken', [
      'http_errors' => FALSE,
      'auth' => [
        $this->config->get('api_key'),
        $this->config->get('api_secret'),
      ],
      'form_params' => $params,
    ]);

    if ($this->config->get('logging_enabled')) {
      $this->logger->info('Attempted access token request. Grant Type - :grant_type, Status - :status', [
        ':grant_type' => $params['grant_type'],
        ':status' => $response->getStatusCode(),
      ]);
    }

    return $response;
  }

}
