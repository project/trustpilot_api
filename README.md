CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/trustpilot_api

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/trustpilot_api


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Trustpilot API module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Web Services >
    Trustpilot API Settings. Provide settings values. Save configurations.

Configurable parameters:
 * API Key.
 * Default Business Unit ID.


MAINTAINERS
-----------

 * Jonathan Daggerhart - https://www.drupal.org/u/daggerhart

Supporting organization:

 * Daggerhart Lab - https://www.drupal.org/daggerhart-lab


EXAMPLE
-------

Using the client service, get the Trustpilot Endpoint plugin instance desired.
Then use the client to perform a request to that endpoint, passing in additional
Endpoint options as the second `request()` method parameter.

```php
/** @var \Drupal\trustpilot_api\TrustpilotApiClientInterface $client */
$client = \Drupal::service('trustpilot_api.client');
$endpoint = $client->getEndpointPluginManager()->createInstance('business_unit_reviews');
$client->request($endpoint, [
  'perPage' => 3,
  'stars' => [3,4,5],
]);
```
